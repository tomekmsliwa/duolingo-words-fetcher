#!/usr/bin/python
""" duolingo.com words fetcher """
import sys, os
try:
    import pycurl, getpass, json
    if sys.version_info.major == 3:
        from urllib.parse import urlencode
        from io import StringIO
    else:
        from urllib import urlencode
        from StringIO import StringIO
except ImportError:
    print('missing modules, exiting')
    sys.exit(-1)

class DuoWords(object):
    """ Class for keeping list of words from duolingo.com """
    def __init__(self, login_data):
        self.username = login_data[0]
        self.words_json = self.__fetch_json(login_data)

    def get_learning_language(self):
        """ Returns learning language code """
        return self.words_json['learning_language']

    def get_word_count(self):
        """ Returns words count """
        return len(self.words_json['vocab_overview'])

    def get_words(self, part_of_speech='all', skill='all'):
        """ Returns all words """
        lines = []
        for count in range(0, self.get_word_count()-1):
            skill_name = str(self.words_json['vocab_overview'][count]['skill'])
            pos = str(self.words_json['vocab_overview'][count]['pos'])
            if sys.version_info.major == 3:
                word = str(self.words_json['vocab_overview'][count]\
                        ['word_string'].encode('utf-8'), 'utf-8')
            else:
                word = str(self.words_json['vocab_overview'][count]\
                        ['word_string'].encode('utf-8'))
            gender = str(self.words_json['vocab_overview'][count]['gender'])
            if (part_of_speech == 'all' or part_of_speech == pos) and\
               (skill == 'all' or skill == skill_name):
                if gender == 'None':
                    lines.append(word)
                else:
                    lines.append(word + ' (' + gender + ')')
        lines.sort()
        return lines

    def get_parts_of_speech(self):
        """ Returns list of parts of speach """
        lines = set()
        for counter in range(0, self.get_word_count()-1):
            pos = str(self.words_json['vocab_overview'][counter]['pos'])
            lines.add(pos)
        lines = list(lines)
        lines.sort()
        return lines

    def get_nouns(self):
        """ Returns list of nouns """
        return self.get_words('Noun')

    def get_skills(self):
        """ Returns list of skills """
        lines = set()
        for count in range(0, self.get_word_count()-1):
            skill_name = str(self.words_json['vocab_overview'][count]['skill'])
            lines.add(skill_name)
        lines = list(lines)
        lines.sort()
        return lines

    def save_words(self):
        """ Saves words to text.file """
        filename = self.username + '-' + str(self.get_word_count()) + '-' \
                + self.get_learning_language() + "-words.txt"
        if os.path.isfile(filename):
            print('file ' + filename + ' already exists, exiting')
            sys.exit(-1)
        print('saving words to: ' + filename)
        lines = list()
        for part_of_speech in self.get_parts_of_speech():
            lines.append(part_of_speech)
            lines.extend(self.get_words(part_of_speech=part_of_speech))
            lines.append('\r')
        if sys.version_info.major == 3:
            with open(filename, 'w', encoding='utf-8') as text_file:
                for line in lines:
                    text_file.write(line + '\n')
                text_file.close()
        else:
            with open(filename, 'w') as text_file:
                for line in lines:
                    text_file.write(line + '\n')
                text_file.close()

    @classmethod
    def __fetch_json(cls, login_data):
        """ Fetches JSON data from duolingo.com page """
        headers = StringIO()
        body = StringIO()

        curl = pycurl.Curl()
        curl.setopt(curl.URL, 'https://www.duolingo.com/login')
        curl.setopt(curl.HTTPHEADER, ['Accept: application/json'])
        post_data = {"login":login_data[0], "password":login_data[1]}
        curl.setopt(curl.HEADER, 1)
        curl.setopt(curl.NOBODY, 0)
        curl.setopt(curl.HTTPHEADER, \
                ['Content-Type : application/x-www-form-urlencoded'])
        curl.setopt(curl.POST, 1)
        post = urlencode(post_data)
        curl.setopt(curl.POSTFIELDS, post)
        curl.setopt(curl.HEADERFUNCTION, headers.write)
        curl.setopt(curl.WRITEFUNCTION, body.write)
        curl.perform()

        headers.seek(0)
        line = headers.readline()
        while line.find('auth_tkt') == -1:
            line = headers.readline()
            if not line:
                print('connection error, exiting')
                sys.exit(-1)

        token = line[4:line.index(';') + 1]

        print('fetching words list..')
        print('logged in as: ' + login_data[0])
        words = StringIO()
        curl = pycurl.Curl()
        curl.setopt(curl.URL, 'https://www.duolingo.com/vocabulary/overview')
        curl.setopt(curl.HTTPHEADER, [token])
        curl.setopt(curl.WRITEFUNCTION, words.write)
        curl.perform()

        return json.loads(words.getvalue())

def get_login_data():
    """ Gets login data from command line arguments or stdin """
    if len(sys.argv) >= 3:
        username = sys.argv[1]
        password = sys.argv[2]
    if len(sys.argv) == 2:
        username = sys.argv[1]
        password = getpass.getpass("password: ")
    if len(sys.argv) == 1:
        if sys.version_info.major == 3:
            username = input("login: ")
        else:
            username = raw_input("login: ")
        password = getpass.getpass("password: ")
    return (username, password)

print('connecting to duolingo...')
DUO_WORDS = DuoWords(get_login_data())
print('active language: ' + DUO_WORDS.get_learning_language())
print('word count: ' + str(DUO_WORDS.get_word_count()))
DUO_WORDS.save_words()

