# Duolingo.com word list fetcher
Fetches words from [duolingo.com](https://duolingo.com) and saves them to text file.

Requires json, pycurl, urllib, StringIO

usage: ./duolingo.py [login] [password]